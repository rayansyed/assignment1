﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    public enum Status //enumeration created, return when user makes a guess
    {
        High,
        Low,
        Correct
    }

    class Game //main class, define all lists and variabals 
    {
        List<Product> products = new List<Product>();
        int guess;
        int NumberOfTries = 0; //variable will be used as a counter
        List<int> PreviousGuesses = new List<int>();
        



        public Game() //constructor no values need to be passed inside
        {
            
        }

        public void PopulateProductsList() //created the lists of products with description and price, prints all the products in the list but not the price of them

        {
            products.Add(new Product("Xbox", 400));
            products.Add(new Product("Honda Civic", 20000));
            products.Add(new Product("Guitar", 700));
            products.Add(new Product("Video Game", 80));
            products.Add(new Product("iPhone X", 1300));
            foreach (Product p in products)
            {
                Console.WriteLine(p.Description);
            }


        }

        public void Start() //Chooses a radndom product from the list
        {
            Random random = new Random();
            Product objProd;
            objProd = products[random.Next(products.Count)];  
        }
        public void CheckGuess() //prints the name of the product thats being guessed
                                 //checks the users guesses and displays the appropraite enum
                                 //put in a while true loop to keep repeating
                                 //loop breaks once the user guesses the correct price
                                
        {

            Random random = new Random();
            Product objProd;
            objProd = products[random.Next(products.Count)];
            Console.WriteLine("Guess the price of this product");
            Console.WriteLine(objProd.Description);

            guess = int.Parse(Console.ReadLine());
            PreviousGuesses.Add(guess);
            while (true)
            {
                if (guess > objProd.Price)
                {
                    Console.WriteLine(Status.High);
                    NumberOfTries++; //adds to the NumberOfTries Variable each time user guesses
                    guess = int.Parse(Console.ReadLine());
                    PreviousGuesses.Add(guess); //adds to the PreviousGuesses List each time user enters a  value
                }


                else if (guess < objProd.Price)
                {
                    Console.WriteLine(Status.Low);
                    NumberOfTries++;
                    guess = int.Parse(Console.ReadLine());
                    PreviousGuesses.Add(guess);
                }

                else if (guess == objProd.Price)
                {
                    Console.WriteLine(Status.Correct);
                    NumberOfTries++;
                    Console.WriteLine($"number of attempts:{NumberOfTries}!"); //tells users how many tries they took
                    Console.WriteLine("Your Previous Attempts Were:");
                    foreach (int g in PreviousGuesses)
                    {
                        Console.WriteLine(g); //prints all the previous guessed
                    }
                    break;
              
                }
                else
                {
                    Console.WriteLine("invalid input");
                }

            }



        }
  







        

       
        
      




    }
}
