﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    class Product
    {
        private string _description; //created properties for the lists
        private int _price;

        public Product(string description, int price)
        {
            _description = description;
            _price = price;
        }

        public string Description
        {
            get {return _description;}

            set{_description = value;}

        }
        public int Price
        {
            get{return _price;}

            set{_price = value;}
        }
    }
}
