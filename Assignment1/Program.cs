﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment1
{
    class Program
    {
        static void Main(string[] args)
        {   //This is the user interface
            string enter; //variable type

            Console.WriteLine("Welcome to the price is right!!!!"); //message
            Console.WriteLine("Press Enter to Play!");
            enter = Console.ReadLine(); //user presses enter...

            if (enter == "")  // when the user presses enter then the game starts
            {
                Console.WriteLine("The Products for Today are...");
                Game begin = new Game();
                begin.PopulateProductsList(); //calls the ProductList Function which displays the products
                begin.Start(); //picks random product from the list
                begin.CheckGuess(); //checks for the guesses
                Console.WriteLine("Thanks For Playing!!!");
            }

            else
                Console.WriteLine("bye");
           
        }
    }
}
